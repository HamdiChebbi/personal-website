User-agent: *
Disallow:/*.png$
Disallow:/images
Disallow:/php
Disallow:/css
Allow:/
Sitemap: http://www.hamdichebbi.com/sitemap.xml